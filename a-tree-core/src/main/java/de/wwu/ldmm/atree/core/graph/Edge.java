package de.wwu.ldmm.atree.core.graph;

public class Edge {

	private Integer from;
	private Integer to;
	private Arrows arrows;

	public Edge() {
		super();
	}

	public Edge(final Integer from, final Integer to, final Arrows arrows) {
		super();
		this.from = from;
		this.to = to;
		this.arrows = arrows;
	}

	public final Integer getFrom() {
		return from;
	}

	public final void setFrom(final Integer from) {
		this.from = from;
	}

	public final Integer getTo() {
		return to;
	}

	public final void setTo(final Integer to) {
		this.to = to;
	}

	public final Arrows getArrows() {
		return arrows;
	}

	public final void setArrows(final Arrows arrows) {
		this.arrows = arrows;
	}
}
