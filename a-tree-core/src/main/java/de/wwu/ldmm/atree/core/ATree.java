package de.wwu.ldmm.atree.core;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.collect.Iterators;

import de.wwu.ldmm.atree.core.index.GLTEIndex;
import de.wwu.ldmm.atree.core.index.Index;
import de.wwu.ldmm.atree.core.segmentation.SegmentationAlgorithm;

/**
 * the implementation of an A-Tree
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 * @param <R> type of the stored records
 * @param <I> type of the underlying index for segment start
 */
public final class ATree<R, I extends GLTEIndex<Segment<R>>> implements Index<R> {

	/**
	 * the index used to find a segment (4.1.1 Tree Search)
	 */
	private final I segmentIndex;

	/**
	 * creates the {@link SegmentationAlgorithm} used for creation of segments on
	 * insertion buffer overflow or bulk loading (e.g. based on the {@link #error})
	 */
	private final transient Function<Integer, SegmentationAlgorithm<R>> segmentationAlgorithmCreator;

	/**
	 * the error term used for segmentation
	 */
	private final int error;

	/**
	 * maximal number of elements in every {@link Segment#getBuffer() segment's
	 * buffer}
	 */
	private final int bufferSize;

	private int size;

	/**
	 * creates an instance of the {@link ATree}
	 * 
	 * @param segmentIndex                 {@link #segmentIndex}
	 * @param segmentationAlgorithmCreator {@link #segmentationAlgorithmCreator}
	 * @param error                        {@link #error}
	 * @param bufferSize                   {@link #bufferSize}
	 */
	public ATree(final I segmentIndex, final Function<Integer, SegmentationAlgorithm<R>> segmentationAlgorithmCreator,
			final int error, final int bufferSize) {
		super();
		this.segmentIndex = segmentIndex;
		this.segmentationAlgorithmCreator = segmentationAlgorithmCreator;
		this.error = error;
		this.bufferSize = bufferSize;
	}

	@Override
	public R get(final int key) {
		// 4.1.1 Tree Search --O(log segmentCount)
		final Segment<R> segment = segmentIndex.getGreatestLessThanOrEqual(key);
		if (segment == null) {
			return null;
		}
		// 4.1.2 Segment Search (1)
		final double predictedPosition = (key - segment.getStart()) * segment.getSlope();

		// 4.1.2 Segment Search (2)
		final int truePosMin = (int) Math.min(segment.getPoints().size() - 1,
				Math.max(0, Math.ceil(predictedPosition - error)));
		final int truePosMax = (int) Math.min(segment.getPoints().size() - 1,
				Math.max(0, Math.floor(predictedPosition + error)));

		final Point<R> searchPoint = Point.empty(key);

		// points binary search - O(log error)
		final R result = binarySearch(segment.getPoints().subList(truePosMin, truePosMax + 1), searchPoint)//
				// buffer binary search - O(log bufferSize)
				.orElse(binarySearch(segment.getBuffer(), searchPoint)
						// key not found
						.orElse(null));

		return result;
	}

	/**
	 * performs a binary search in a {@link List} of {@link Point points}
	 * 
	 * @param sortedList  {@link List} on which binary search is applied
	 *                    (segment-data/buffer)
	 * @param searchPoint {@link Point} capsule for binary search
	 * @return {@link Optional} for the found {@link Point#getRecord() record}
	 */
	private Optional<R> binarySearch(final List<Point<R>> sortedList, final Point<R> searchPoint) {
		return Optional.ofNullable(Collections.binarySearch(sortedList, searchPoint))//
				.filter(index -> index >= 0)//
				.map(sortedList::get)//
				.map(Point::getRecord);
	}

	@Override
	public boolean insert(final Point<R> point) {
		final Segment<R> segment = segmentIndex.getGreatestLessThanOrEqual(point.getKey());
		if (segment == null) {
			return false;
		}

		segment.getBuffer().add(point);
		if (segment.getBuffer().size() >= bufferSize) {
			final Iterator<Point<R>> pointIterator = Iterators.concat(segment.getPoints().iterator(),
					segment.getBuffer().iterator());
			segmentIndex.delete(point.getKey());
			load(pointIterator);
		}

		size++;

		return true;
	}

	@Override
	public boolean delete(final int key) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * initializes the {@link ATree} based on a potentially large amount of data
	 * 
	 * @param dataIterator iterates over the dataset
	 */
	public void load(final Iterator<Point<R>> dataIterator) {
		final SegmentationAlgorithm<R> segmentationAlgorithm = segmentationAlgorithmCreator.apply(error);
		final List<Segment<R>> newSegments = segmentationAlgorithm.run(dataIterator);
		newSegments.stream()//
				.map(Segment::createPoint)//
				.forEach(segmentIndex::insert);
		size = newSegments.stream().mapToInt(s -> s.getPoints().size()).sum();
	}

	public int getError() {
		return error;
	}

	@Override
	public int size() {
		return size;
	}

	/**
	 * @return {@link #segmentIndex}
	 */
	public I getSegmentIndex() {
		return segmentIndex;
	}
}
