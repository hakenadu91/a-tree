package de.wwu.ldmm.atree.core.bptree;

import de.wwu.ldmm.atree.core.index.HasKey;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 * @param <R> type of the stored records
 */
public final class BPInternalTreeNodePointer<R> implements HasKey {

	/**
	 * the key entry
	 */
	private int key;

	/**
	 * the left child node (all contained keys smaller than {@link #key})
	 */
	private BPTreeNode<?, R> left;

	/**
	 * the right child node (all contained keys greater or equal to {@link #key})
	 */
	private BPTreeNode<?, R> right;

	/**
	 * @return {@link #key}
	 */
	@Override
	public int getKey() {
		return key;
	}

	/**
	 * @param key {@link #key}
	 */
	public void setKey(final int key) {
		this.key = key;
	}

	/**
	 * @return {@link #left}
	 */
	public BPTreeNode<?, R> getLeft() {
		return left;
	}

	/**
	 * @param left new value for {@link #left}
	 */
	public void setLeft(final BPTreeNode<?, R> left) {
		this.left = left;
	}

	/**
	 * @return {@link #right}
	 */
	public BPTreeNode<?, R> getRight() {
		return right;
	}

	/**
	 * @param right new value for {@link #right}
	 */
	public void setRight(final BPTreeNode<?, R> right) {
		this.right = right;
	}

	public static <R> BPInternalTreeNodePointer<R> empty(final int key) {
		final BPInternalTreeNodePointer<R> pointer = new BPInternalTreeNodePointer<>();
		pointer.setKey(key);
		return pointer;
	}
}
