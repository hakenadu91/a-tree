package de.wwu.ldmm.atree.core.analysis;

import de.wwu.ldmm.atree.core.ATree;
import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.bptree.BPLeafTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPTree;

public final class ATreeAnalysis {

	private double numberOfSegments;
	private double numberOfSmallSegments;
	private double height;

	public ATreeAnalysis(final ATree<Integer, BPTree<Segment<Integer>>> aTree) {
		super();
		initialize(aTree);
	}

	private void initialize(final ATree<Integer, BPTree<Segment<Integer>>> aTree) {
		height = aTree.getSegmentIndex().getHeight();
		BPLeafTreeNode<Segment<Integer>> leaf = aTree.getSegmentIndex().getFirstLeaf();
		while (leaf != null) {
			for (final Point<Segment<Integer>> segmentPoint : leaf.getChildren()) {
				numberOfSegments++;
				final Segment<Integer> segment = segmentPoint.getRecord();
				if (segment == null || segment.getPoints() == null) {
					continue;
				}
				if (segment.getPoints().size() <= aTree.getError()) {
					numberOfSmallSegments++;
				}
			}
			leaf = leaf.getNeighbourLeaf();
		}
	}

	public double getNumberOfSegments() {
		return numberOfSegments;
	}

	public double getNumberOfSmallSegments() {
		return numberOfSmallSegments;
	}

	public double getHeight() {
		return height;
	}
}
