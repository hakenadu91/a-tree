package de.wwu.ldmm.atree.core.json;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.wwu.ldmm.atree.core.bptree.BPInternalTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPInternalTreeNodePointer;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 03.05.2019
 */
public final class BPInternalTreeNodeSerializer implements JsonSerializer<BPInternalTreeNode<?>> {

	@Override
	public JsonElement serialize(final BPInternalTreeNode<?> src, final Type typeOfSrc,
			final JsonSerializationContext context) {
		final JsonObject result = new JsonObject();

		final JsonArray keys = new JsonArray();
		final JsonArray children = new JsonArray();
		for (final BPInternalTreeNodePointer<?> pointer : src.getChildren()) {
			if (src.getChildren().indexOf(pointer) == 0) {
				children.add(context.serialize(pointer.getLeft()));
			}
			keys.add(new JsonPrimitive(pointer.getKey()));
			children.add(context.serialize(pointer.getRight()));
		}
		result.add("pointers", keys);
		result.add("children", children);

		return result;
	}
}
