package de.wwu.ldmm.atree.core.cost;

public final class SizeCostFunction<R> implements CostFunction<R> {

	private final double fillRatio;

	public SizeCostFunction(final double fillRatio) {
		super();
		this.fillRatio = fillRatio;
	}

	@Override
	public double calculateCost(final int segmentCount, final int error, final int fanout) {
		return fillRatio * segmentCount * (Math.log(segmentCount) / Math.log(fanout)) * 16 + segmentCount * 24;
	}
}
