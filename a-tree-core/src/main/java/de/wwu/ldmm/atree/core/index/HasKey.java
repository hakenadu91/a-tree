package de.wwu.ldmm.atree.core.index;

/**
 * for static typing support in index structures
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 02.05.2019
 */
public interface HasKey extends Comparable<HasKey> {

	/**
	 * @return the integer key value which can be used by index structures
	 */
	public int getKey();

	@Override
	default int compareTo(final HasKey other) {
		return Integer.compare(getKey(), other.getKey());
	}
}
