package de.wwu.ldmm.atree.core.graph;

public class Node {

	private Integer id;
	private String label;
	private Integer level;

	public Node() {
		super();
	}

	public Node(final Integer id, final String label, final Integer level) {
		super();
		this.id = id;
		this.label = label;
		this.level = level;
	}

	public final Integer getId() {
		return id;
	}

	public final void setId(final Integer id) {
		this.id = id;
	}

	public final String getLabel() {
		return label;
	}

	public final void setLabel(final String label) {
		this.label = label;
	}

	public final Integer getLevel() {
		return level;
	}

	public final void setLevel(final Integer level) {
		this.level = level;
	}
}
