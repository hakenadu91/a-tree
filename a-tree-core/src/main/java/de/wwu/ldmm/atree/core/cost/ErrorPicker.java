package de.wwu.ldmm.atree.core.cost;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.segmentation.SegmentationAlgorithm;

public class ErrorPicker<R> {

	private final CostFunction<R> primaryCostFunction;
	private final CostFunction<R> secondaryCostFunction;

	public ErrorPicker(final CostFunction<R> primaryCostFunction, final CostFunction<R> secondaryCostFunction) {
		super();
		this.primaryCostFunction = primaryCostFunction;
		this.secondaryCostFunction = secondaryCostFunction;
	}

	public int pickError(final Iterator<Point<R>> points,
			final Function<Integer, SegmentationAlgorithm<R>> segmentationAlgorithmFunction, final int fanout,
			final double req, int... errors) {
		final Map<Integer, Double> costs = new HashMap<>();
		for (final int error : errors) {
			final double cost = primaryCostFunction.calculateCost(points, segmentationAlgorithmFunction, error, fanout);
			if (cost <= req) {
				costs.put(error, cost);
			}
		}

		if (costs.isEmpty()) {
			return -1;
		}

		int minKey = -1;
		double minSecondary = Double.POSITIVE_INFINITY;
		for (final Entry<Integer, Double> entry : costs.entrySet()) {
			final int key = entry.getKey();
			final double secondary = secondaryCostFunction.calculateCost(points, segmentationAlgorithmFunction, key,
					fanout);
			if (minKey == -1 || secondary < minSecondary) {
				minKey = key;
				minSecondary = secondary;
			}
		}

		return minKey;
	}
}
