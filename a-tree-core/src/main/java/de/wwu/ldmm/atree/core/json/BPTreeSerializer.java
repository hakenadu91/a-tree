package de.wwu.ldmm.atree.core.json;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.wwu.ldmm.atree.core.bptree.BPTree;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 03.05.2019
 */
public final class BPTreeSerializer implements JsonSerializer<BPTree<?>> {

	@Override
	public JsonElement serialize(final BPTree<?> src, final Type typeOfSrc, final JsonSerializationContext context) {
		final JsonObject result = new JsonObject();
		result.add("degree", context.serialize(src.getB()));
		result.add("root", context.serialize(src.getRoot()));
		return result;
	}
}
