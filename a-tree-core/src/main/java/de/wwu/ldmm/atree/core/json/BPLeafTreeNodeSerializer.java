package de.wwu.ldmm.atree.core.json;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.bptree.BPLeafTreeNode;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 03.05.2019
 */
public final class BPLeafTreeNodeSerializer implements JsonSerializer<BPLeafTreeNode<?>> {

	@Override
	public JsonElement serialize(final BPLeafTreeNode<?> src, final Type typeOfSrc,
			final JsonSerializationContext context) {
		final JsonArray result = new JsonArray();
		src.getChildren().stream()//
				.map(Point::getRecord)//
				.map(context::serialize)//
				.forEach(result::add);
		return result;
	}
}
