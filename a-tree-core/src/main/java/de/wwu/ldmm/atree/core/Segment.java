package de.wwu.ldmm.atree.core;

import java.util.ArrayList;
import java.util.List;

import de.wwu.ldmm.atree.core.json.ATreeGson;

/**
 * The A-Tree's main idea is to apply a segmentation to a set of data points and
 * only store start and slope of every segment in an underlying (conventional)
 * index structure. An instance of this class represents one segment.
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 */
public final class Segment<R> {

	/**
	 * slope of the linear function which is used for position approximation
	 */
	private double slope;

	/**
	 * {@link List} of data points whose positions may be approximated by the linear
	 * function created by the first point in this {@link List} and the
	 * {@link #slope}
	 */
	private List<Point<R>> points;

	/**
	 * The buffer is used for insert operations (5-Index Inserts). It is kept sorted
	 * for search and merge operations and is merged into the {@link #points} when
	 * it is full.
	 */
	private final List<Point<R>> buffer;

	/**
	 * creates an instance of the {@link Segment}
	 */
	public Segment() {
		super();
		this.buffer = new ArrayList<>();
	}

	/**
	 * @return {@link #slope}
	 */
	public double getSlope() {
		return slope;
	}

	/**
	 * @param slope new {@link #slope}
	 */
	public void setSlope(final double slope) {
		this.slope = slope;
	}

	/**
	 * @return key of the first element in {@link #points}
	 */
	public int getStart() {
		if (points == null || points.isEmpty()) {
			throw new IllegalStateException("no points => no start");
		}
		return points.get(0).getKey();
	}

	/**
	 * @return {@link #points}
	 */
	public List<Point<R>> getPoints() {
		return points;
	}

	/**
	 * @param points new {@link #points}
	 */
	public void setPoints(final List<Point<R>> points) {
		this.points = points;
	}

	/**
	 * @return {@link #buffer}
	 */
	public List<Point<R>> getBuffer() {
		return buffer;
	}

	public Point<Segment<R>> createPoint() {
		return new Point<>(getStart(), this);
	}

	public Point<R> getOrigin() {
		return points.isEmpty() ? null : points.get(0);
	}

	@Override
	public String toString() {
		return ATreeGson.INSTANCE.toJson(this);
	}
}
