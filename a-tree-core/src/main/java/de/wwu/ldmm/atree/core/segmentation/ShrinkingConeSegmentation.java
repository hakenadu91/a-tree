package de.wwu.ldmm.atree.core.segmentation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.json.ATreeGson;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 */
public final class ShrinkingConeSegmentation<R> implements SegmentationAlgorithm<R> {

	private final int error;

	public ShrinkingConeSegmentation(final int error) {
		super();
		this.error = error;
	}

	public static void main(String[] args) {
		while (true) {
			final List<Point<Integer>> points = new ArrayList<>();
			int value = 0;
			for (int i = 0; i < 100; i++) {
				value += 1 + new Random().nextInt(10);
				points.add(new Point<>(value, i));
			}
			final List<Segment<Integer>> segments = new ShrinkingConeSegmentation<Integer>(3).run(points.iterator());
			if (segments.get(0).getPoints().size() < 20 && segments.size() < 15) {
				System.err.println(segments.get(0).getPoints().size());
				System.out.println(points.size() + ": {"
						+ points.stream().map(Point::getKey).map(Object::toString).collect(Collectors.joining(", "))
						+ '}');
				System.out.println(segments.size() + ": {" + segments.stream().map(Segment::getStart)
						.map(Object::toString).collect(Collectors.joining(", ")) + '}');
				System.out.println(segments.stream()//
						.map(s -> "(" + s.getStart() + ',' + s.getSlope() + ')')//
						.collect(Collectors.joining(", ")));
				System.out.println(ATreeGson.INSTANCE.toJson(segments));

				final BPTree<Integer> bpTree = new BPTree<>(3);
				segments.stream().map(s -> s.getPoints().get(0)).forEach(bpTree::insert);
				System.out.println(ATreeGson.INSTANCE.toJson(bpTree));
				break;
			}
		}
	}

	@Override
	public List<Segment<R>> run(final Iterator<Point<R>> points) {
		final List<Segment<R>> segments = new LinkedList<>();

		double slopeHigh = Double.POSITIVE_INFINITY;
		double slopeLow = 0D;
		Segment<R> currentSegment = null;
		int position = 0;

		while (points.hasNext()) {
			final Point<R> point = points.next();
			if (currentSegment == null) {
				currentSegment = createSegment(point);
			} else {
				final double x = point.getKey() - currentSegment.getOrigin().getKey();
				final double nextSlopeHigh = Math.max(slopeLow, Math.min(slopeHigh, (position + error) / x));
				final double nextSlopeLow = Math.min(slopeHigh, Math.max(slopeLow, (position - error) / x));
				if (isInCone(currentSegment.getOrigin(), point, position, nextSlopeHigh, nextSlopeLow)) {
					currentSegment.getPoints().add(point);
					slopeHigh = nextSlopeHigh;
					slopeLow = nextSlopeLow;
					if (!points.hasNext()) {
						currentSegment.setSlope((slopeHigh - slopeLow) / 2 + slopeLow);
					}
				} else {
					currentSegment.setSlope((slopeHigh - slopeLow) / 2 + slopeLow);
					segments.add(currentSegment);

					slopeHigh = Double.POSITIVE_INFINITY;
					slopeLow = 0D;
					currentSegment = createSegment(point);
					position = 0;
				}
			}
			position++;
		}

		if (currentSegment != null && !currentSegment.getPoints().isEmpty()) {
			segments.add(currentSegment);
		}

		return segments;
	}

	private Segment<R> createSegment(final Point<R> point) {
		final Segment<R> segment = new Segment<>();
		segment.setPoints(new ArrayList<>());
		segment.getPoints().add(point);
		return segment;
	}

	private boolean isInCone(//
			final Point<R> origin, //
			final Point<R> point, //
			final int position, //
			final double slopeHigh, //
			final double slopeLow) {

		if (slopeHigh == Double.POSITIVE_INFINITY && slopeLow == 0D) {
			return true;
		}

		final double upperBound = (point.getKey() - origin.getKey()) * slopeHigh;
		if (upperBound < position) {
			return false;
		}

		final double lowerBound = (point.getKey() - origin.getKey()) * slopeLow;
		return lowerBound <= position;
	}
}
