package de.wwu.ldmm.atree.core;

import de.wwu.ldmm.atree.core.index.HasKey;
import de.wwu.ldmm.atree.core.index.Index;
import de.wwu.ldmm.atree.core.json.ATreeGson;

/**
 * An instance of this class contains an integer key and a record of any type.
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 * @param <R> type of the stored record
 */
public final class Point<R> implements HasKey {

	/**
	 * integer value which is used by {@link Index} structures
	 */
	private int key;

	/**
	 * value which may be retrieved by {@link Index}
	 * 
	 * @see Index#get(int)
	 */
	private R record;

	/**
	 * @param key    {@link #key}
	 * @param record {@link #record}
	 */
	public Point(final int key, final R record) {
		super();
		this.key = key;
		this.record = record;
	}

	/**
	 * @return {@link #key}
	 */
	@Override
	public int getKey() {
		return key;
	}

	/**
	 * @param key new value for {@link #key}
	 */
	public void setKey(final int key) {
		this.key = key;
	}

	/**
	 * @return {@link #record}
	 */
	public R getRecord() {
		return record;
	}

	/**
	 * @param record new value for {@link #record}
	 */
	public void setRecord(final R record) {
		this.record = record;
	}

	/**
	 * creates an empty {@link Point} instance for binary search purposes
	 * 
	 * @param key {@link #key}
	 * @return new {@link Point} instance without {@link #record}
	 */
	public static <R> Point<R> empty(final int key) {
		return new Point<>(key, null);
	}

	@Override
	public String toString() {
		return ATreeGson.INSTANCE.toJson(this);
	}
}
