package de.wwu.ldmm.atree.core.bptree;

import java.util.Collections;
import java.util.Optional;

import de.wwu.ldmm.atree.core.Point;

/**
 * leaf node of the {@link BPTree}
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 * @param <R> type of the stored records
 */
public final class BPLeafTreeNode<R> extends BPTreeNode<Point<R>, R> {

	/**
	 * each leaf points to the leaf to the right
	 */
	private BPLeafTreeNode<R> neighbourLeaf;

	/**
	 * creates an instance of the {@link BPLeafTreeNode}
	 * 
	 * @param tree {@link #getTree()}
	 */
	public BPLeafTreeNode(final BPTree<R> tree) {
		super(tree);
	}

	@Override
	boolean insert(final Point<R> point) {
		addChild(point);
		return true;
	}

	@Override
	boolean delete(final int key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	R get(final int key) {
		for (final Point<R> child : getChildren()) {
			if (child.getKey() == key) {
				return child.getRecord();
			}
		}
		return null;
	}

	@Override
	R getGreatestLessThanOrEqual(final int key) {
		if (getChildren().isEmpty()) {
			return null;
		}
		Point<R> previous = null;
		for (final Point<R> child : getChildren()) {
			if (child.getKey() == key) {
				return child.getRecord();
			}
			if (child.getKey() >= key) {
				return Optional.ofNullable(previous)//
						.map(Point::getRecord)//
						.orElse(null);
			}
			previous = child;
		}
		return getChildren().get(getChildren().size() - 1).getRecord();
	}

	@Override
	protected void addChildInternal(final Point<R> child) {
		for (int insertionIndex = 0; insertionIndex < getChildren().size(); insertionIndex++) {
			if (getChildren().get(insertionIndex).getKey() >= child.getKey()) {
				getChildren().add(insertionIndex, child);
				return;
			}
		}
		getChildren().add(child);
	}

	@Override
	protected boolean removeChildInternal(final Point<R> child) {
		final int index = Collections.binarySearch(getChildren(), child);
		if (index == -1) {
			return false;
		}
		getChildren().remove(index);
		return true;
	}

	@Override
	protected BPLeafTreeNode<R> createNewNode() {
		final BPLeafTreeNode<R> newNode = new BPLeafTreeNode<>(getTree());
		setNeighbourLeaf(newNode);
		return newNode;
	}

	@Override
	protected boolean isLeaf() {
		return true;
	}

	@Override
	protected BPLeafTreeNode<R> getFirstLeaf() {
		return this;
	}

	/**
	 * @return {@link #neighbourLeaf}
	 */
	public BPLeafTreeNode<R> getNeighbourLeaf() {
		return neighbourLeaf;
	}

	/**
	 * @param neighbourLeaf new value for {@link #neighbourLeaf}
	 */
	public void setNeighbourLeaf(final BPLeafTreeNode<R> neighbourLeaf) {
		this.neighbourLeaf = neighbourLeaf;
	}
}
