package de.wwu.ldmm.atree.core.bptree;

import java.util.Objects;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.index.GLTEIndex;
import de.wwu.ldmm.atree.core.json.ATreeGson;

/**
 * B+ tree implementation
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 26.04.2019
 * @param <R> type of the stored records
 */
public final class BPTree<R> implements GLTEIndex<R> {

	/**
	 * this value determines the number of children for each
	 * {@link BPInternalTreeNode}
	 */
	private final int b;

	/**
	 * the B+ tree's root node
	 */
	private BPTreeNode<?, R> root;

	/**
	 * the B+ tree's height
	 */
	private int height = 1;

	/**
	 * the B+ tree's size
	 */
	private int size = 0;

	/**
	 * creates an instance of {@link BPTree}
	 * 
	 * @param b {@link #b}
	 */
	public BPTree(final int b) {
		super();
		if (b < 3) {
			throw new IllegalArgumentException("b must be >= 3");
		}
		this.b = b;
	}

	@Override
	public R get(final int key) {
		if (root == null) {
			return null;
		}
		return root.get(key);
	}

	@Override
	public R getGreatestLessThanOrEqual(final int key) {
		if (root == null) {
			return null;
		}
		return root.getGreatestLessThanOrEqual(key);
	}

	static int count = 0;

	@Override
	public boolean insert(final Point<R> point) {
		Objects.requireNonNull(point, "null passed instead of point");
		if (root == null) {
			root = new BPLeafTreeNode<>(this);
		}
		final boolean inserted = root.insert(point);
		if (inserted) {
			size++;
		}
		return inserted;
	}

	@Override
	public boolean delete(final int key) {
		if (root == null) {
			return false;
		}
		final boolean deleted = root.delete(key);
		if (deleted) {
			size--;
		}
		return deleted;
	}

	public final BPLeafTreeNode<R> getFirstLeaf() {
		if (root == null) {
			return null;
		}
		return root.getFirstLeaf();
	}

	/**
	 * @param root new {@link BPTreeNode} for {@link #root}
	 */
	void setRoot(final BPTreeNode<?, R> root) {
		this.root = root;
	}

	/**
	 * @return {@link #root}
	 */
	public BPTreeNode<?, R> getRoot() {
		return root;
	}

	/**
	 * @return {@link #b}
	 */
	public int getB() {
		return b;
	}

	void setHeight(final int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public String toString() {
		return ATreeGson.INSTANCE.toJson(this);
	}

	@Override
	public int size() {
		return size;
	}
}
