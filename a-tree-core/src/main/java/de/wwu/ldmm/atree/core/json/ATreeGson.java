package de.wwu.ldmm.atree.core.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.wwu.ldmm.atree.core.bptree.BPInternalTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPLeafTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPTree;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 */
public final class ATreeGson {

	public static final Gson INSTANCE = new GsonBuilder()//
			.registerTypeHierarchyAdapter(BPTree.class, new BPTreeSerializer())//
			.registerTypeHierarchyAdapter(BPInternalTreeNode.class, new BPInternalTreeNodeSerializer())//
			.registerTypeHierarchyAdapter(BPLeafTreeNode.class, new BPLeafTreeNodeSerializer())//
			.create();
}
