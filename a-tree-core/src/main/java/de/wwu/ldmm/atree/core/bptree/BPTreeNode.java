package de.wwu.ldmm.atree.core.bptree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.index.HasKey;
import de.wwu.ldmm.atree.core.json.ATreeGson;

/**
 * provides abstraction for the following nodes:
 * <ul>
 * <li>{@link BPInternalTreeNode}</li>
 * <li>{@link BPLeafTreeNode}</li>
 * </ul>
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 26.04.2019
 * @param <C> type of the elements in this node's {@link #children}
 * @param <R> type of the stored records
 */
public abstract class BPTreeNode<C extends HasKey, R> {

	/**
	 * the {@link BPTree} which contains this {@link BPTreeNode}
	 */
	private final BPTree<R> tree;

	/**
	 * contains values or subordinate nodes (depending on the specification)
	 */
	private List<C> children;

	/**
	 * the parent node of every {@link BPTreeNode} is either <code>null</code> (if
	 * this node is the root of the {@link BPTree}) or an instance of
	 * {@link BPInternalTreeNode}
	 */
	private BPInternalTreeNode<R> parent;

	/**
	 * creates an instance of {@link BPTreeNode}
	 * 
	 * @param tree {@link #tree}
	 */
	protected BPTreeNode(final BPTree<R> tree) {
		super();
		this.tree = tree;
	}

	protected final boolean isMinimal() {
		return isUnderfilled(children.size() - 1);
	}

	protected final boolean isUnderfilled() {
		return isUnderfilled(getChildren().size());
	}

	protected final boolean isUnderfilled(final int childrenCount) {
		if (isRoot()) {
			if (isLeaf()) {
				return childrenCount < 1;
			}
			return childrenCount < 2;
		}
		return childrenCount < tree.getB() / 2;
	}

	protected final boolean isMaximal() {
		return isOverfilled(children.size() + 1);
	}

	protected final boolean isOverfilled() {
		return isOverfilled(getChildren().size());
	}

	protected final boolean isOverfilled(final int childrenCount) {
		if (isRoot() && isLeaf()) {
			return childrenCount > getTree().getB() - 1;
		}
		return childrenCount > getTree().getB();
	}

	protected abstract boolean isLeaf();

	/**
	 * inserts a {@link Point} into the subtree created by this {@link BPTreeNode}
	 * 
	 * @param point {@link Point} to insert
	 * @return <code>true</code> on successful insertion
	 */
	abstract boolean insert(Point<R> point);

	/**
	 * deletes a {@link Point} from the subtree created by this {@link BPTreeNode}
	 * 
	 * @param point {@link Point} to delete
	 * @return <code>true</code> on successful deletion
	 */
	abstract boolean delete(int key);

	/**
	 * get the position stored for a specific key
	 * 
	 * @param key {@link Point#getKey()}
	 * @return {@link Point#getRecord()} for the passed key or <code>null</code>
	 */
	abstract R get(int key);

	abstract R getGreatestLessThanOrEqual(int key);

	protected abstract void addChildInternal(C child);

	protected boolean removeChildInternal(C child) {
		final boolean removed = getChildren().remove(child);
		if (!removed) {
			return false;
		}
		return true;
	}

	protected abstract BPTreeNode<C, R> createNewNode();

	protected abstract BPLeafTreeNode<R> getFirstLeaf();

	protected final void removeChild(C child) {
		removeChildInternal(child);
		if (isUnderfilled()) {
			handleUnderfill();
		}
	}

	protected final void addChild(C child) {
		addChildInternal(child);
		if (isOverfilled()) {
			handleOverfill();
		}
	}

	private void handleUnderfill() {

	}

	@SuppressWarnings("unchecked")
	private void handleOverfill() {
		final BPTreeNode<C, R> newNode = createNewNode();

		IntStream.range(getChildren().size() / 2, getChildren().size())//
				.mapToObj(getChildren()::get)//
				.forEach(newNode::registerChild);

		getChildren().removeAll(newNode.getChildren());

		final BPInternalTreeNodePointer<R> pointer = new BPInternalTreeNodePointer<>();
		if (isLeaf()) {
			pointer.setKey(newNode.getChildren().get(0).getKey());
		} else {
			final BPInternalTreeNodePointer<R> removedPointer = (BPInternalTreeNodePointer<R>) newNode.getChildren()
					.remove(0);
			pointer.setKey(removedPointer.getKey());
			removedPointer.getLeft().setParent((BPInternalTreeNode<R>) this);
		}
		pointer.setLeft(this);
		pointer.setRight(newNode);

		if (isRoot()) {
			final BPInternalTreeNode<R> newRoot = new BPInternalTreeNode<>(getTree());
			newRoot.addChild(pointer);
			getTree().setRoot(newRoot);
			setParent(newRoot);
			getTree().setHeight(getTree().getHeight() + 1);
		} else {
			getParent().addChild(pointer);
		}
		newNode.setParent(getParent());
	}

	/**
	 * @return <code>true</code> when this {@link BPTreeNode} is the root of the
	 *         {@link #tree}
	 */
	protected final boolean isRoot() {
		return getParent() == null;
	}

	/**
	 * @return {@link #parent}
	 */
	public final BPInternalTreeNode<R> getParent() {
		return parent;
	}

	/**
	 * @param parent new {@link BPInternalTreeNode} for {@link #parent}
	 */
	public final void setParent(final BPInternalTreeNode<R> parent) {
		this.parent = parent;
	}

	protected void registerChild(final C child) {
		getChildren().add(child);
	}

	/**
	 * @return {@link #children}
	 */
	public final List<C> getChildren() {
		if (children == null) {
			children = new ArrayList<>();
		}
		return children;
	}

	/**
	 * @param children {@link #children}
	 */
	protected final void setChildren(final List<C> children) {
		this.children = children;
	}

	/**
	 * @return {@link #tree}
	 */
	protected final BPTree<R> getTree() {
		return tree;
	}

	@Override
	public String toString() {
		return ATreeGson.INSTANCE.toJson(this);
	}
}
