package de.wwu.ldmm.atree.core.cost;

import java.util.Iterator;
import java.util.function.Function;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.segmentation.SegmentationAlgorithm;

public interface CostFunction<R> {

	double calculateCost(int segmentCount, int error, int fanout);

	default double calculateCost(Iterator<Point<R>> points,
			Function<Integer, SegmentationAlgorithm<R>> segmentationAlgorithmFunction, int error, int fanout) {
		return calculateCost(segmentationAlgorithmFunction.apply(error).run(points).size(), error, fanout);
	}
}
