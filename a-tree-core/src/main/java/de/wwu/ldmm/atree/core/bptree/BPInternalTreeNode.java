package de.wwu.ldmm.atree.core.bptree;

import java.util.Iterator;
import java.util.function.BiFunction;

import de.wwu.ldmm.atree.core.Point;

/**
 * internal node of the {@link BPTree}
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 * @param <R> type of the stored records
 */
public final class BPInternalTreeNode<R> extends BPTreeNode<BPInternalTreeNodePointer<R>, R> {

	/**
	 * creates an instance of the {@link BPInternalTreeNode}
	 * 
	 * @param tree {@link #getTree()}
	 */
	public BPInternalTreeNode(final BPTree<R> tree) {
		super(tree);
	}

	@Override
	boolean insert(final Point<R> point) {
		final Iterator<BPInternalTreeNodePointer<R>> pointerIterator = getChildren().iterator();
		while (pointerIterator.hasNext()) {
			final BPInternalTreeNodePointer<R> pointer = pointerIterator.next();
			if (pointer.getKey() > point.getKey()) {
				return pointer.getLeft().insert(point);
			}
			if (!pointerIterator.hasNext()) {
				return pointer.getRight().insert(point);
			}
		}
		return false;
	}

	@Override
	boolean delete(final int key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	R get(final int key) {
		return get(key, BPTreeNode::get);
	}

	@Override
	R getGreatestLessThanOrEqual(int key) {
		return get(key, BPTreeNode::getGreatestLessThanOrEqual);
	}

	private R get(final int key, final BiFunction<BPTreeNode<?, R>, Integer, R> getter) {
		if (getChildren().isEmpty()) {
			return null;
		}
		for (final BPInternalTreeNodePointer<R> child : getChildren()) {
			if (child.getKey() > key) {
				return getter.apply(child.getLeft(), key);
			}
		}
		return getter.apply(getChildren().get(getChildren().size() - 1).getRight(), key);
	}

	@Override
	protected void addChildInternal(final BPInternalTreeNodePointer<R> child) {
		for (int insertionIndex = 0; insertionIndex < getChildren().size(); insertionIndex++) {
			final BPInternalTreeNodePointer<R> pointerAtIndex = getChildren().get(insertionIndex);
			if (pointerAtIndex.getKey() >= child.getKey()) {
				pointerAtIndex.setLeft(child.getRight());
				if (insertionIndex > 0) {
					final BPInternalTreeNodePointer<R> pointerToTheLeft = getChildren().get(insertionIndex - 1);
					pointerToTheLeft.setRight(child.getLeft());
				}
				getChildren().add(insertionIndex, child);
				return;
			}
		}
		if (!getChildren().isEmpty()) {
			getChildren().get(getChildren().size() - 1).setRight(child.getLeft());
		}
		getChildren().add(child);
	}

	@Override
	protected boolean removeChildInternal(final BPInternalTreeNodePointer<R> child) {
		if (!super.removeChildInternal(child)) {
			return false;
		}
		// TODO
		return true;
	}

	@Override
	protected BPLeafTreeNode<R> getFirstLeaf() {
		return getChildren().get(0).getLeft().getFirstLeaf();
	}

	@Override
	protected BPTreeNode<BPInternalTreeNodePointer<R>, R> createNewNode() {
		return new BPInternalTreeNode<>(getTree());
	}

	@Override
	protected void registerChild(final BPInternalTreeNodePointer<R> child) {
		super.registerChild(child);
		child.getLeft().setParent(this);
		child.getRight().setParent(this);
	}

	@Override
	protected final boolean isLeaf() {
		return false;
	}
}
