package de.wwu.ldmm.atree.core.segmentation;

import java.util.Iterator;
import java.util.List;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 */
@FunctionalInterface
public interface SegmentationAlgorithm<R> {

	List<Segment<R>> run(final Iterator<Point<R>> points);
}
