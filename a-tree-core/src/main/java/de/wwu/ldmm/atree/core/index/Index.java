package de.wwu.ldmm.atree.core.index;

import de.wwu.ldmm.atree.core.Point;

/**
 * superinterface for every index structure (A-Tree, B+-Tree etc.)
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 28.04.2019
 * @param <R> type of the stored record
 */
public interface Index<R> {

	/**
	 * lookup an element by its key
	 * 
	 * @param key element key
	 * @return element value
	 */
	R get(int key);

	/**
	 * insert an element into the index
	 * 
	 * @param point (key,value)-pair
	 * @return <code>true</code> on successful insertion
	 */
	boolean insert(Point<R> point);

	/**
	 * deletes an element from the index
	 * 
	 * @param key element key
	 * @return <code>true</code> on successful deletion
	 */
	boolean delete(int key);

	/**
	 * @return amount of inserted elements
	 */
	int size();

	/**
	 * searches for an element in the index and returns <code>true</code> when it
	 * was found
	 * 
	 * @param key element key
	 * @return <code>true</code> when the element key was found
	 */
	default boolean contains(final int key) {
		return get(key) != null;
	}
}
