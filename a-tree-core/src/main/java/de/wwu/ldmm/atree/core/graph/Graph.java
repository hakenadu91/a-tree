package de.wwu.ldmm.atree.core.graph;

import java.util.LinkedList;
import java.util.List;

public class Graph {

	private List<Node> nodes;
	private List<Edge> edges;

	public final void addNode(final Node node) {
		if (nodes == null) {
			setNodes(new LinkedList<>());
		}
		nodes.add(node);
	}

	public final void addEdge(final Edge edge) {
		if (edges == null) {
			setEdges(new LinkedList<>());
		}
		edges.add(edge);
	}

	public final List<Node> getNodes() {
		return nodes;
	}

	public final void setNodes(final List<Node> nodes) {
		this.nodes = nodes;
	}

	public final List<Edge> getEdges() {
		return edges;
	}

	public final void setEdges(final List<Edge> edges) {
		this.edges = edges;
	}
}
