package de.wwu.ldmm.atree.core.cost;

public final class LatencyCostFunction<R> implements CostFunction<R> {

	private final double cacheMissPenalty;
	private final double bufferSize;

	public LatencyCostFunction(final int cacheMissPenalty, final int bufferSize) {
		super();
		this.cacheMissPenalty = cacheMissPenalty;
		this.bufferSize = bufferSize;
	}

	@Override
	public double calculateCost(final int segmentCount, final int error, final int fanout) {
		return cacheMissPenalty * (Math.log(segmentCount) / Math.log(fanout)//
				+ Math.log(error) / Math.log(2)//
				+ Math.log(bufferSize) / Math.log(2));
	}
}
