package de.wwu.ldmm.atree.core.index;

/**
 * {@link Index} which additionally supports to seek for the greatest key which
 * is less or equal to a passed lookup key
 * 
 * @author Manuel Seiche
 * @since 30.04.2019
 * @param <R> type of the stored records
 */
public interface GLTEIndex<R> extends Index<R> {

	/**
	 * @param key lookup key
	 * @return greatest found key which is less than or equal to <code>key</code>
	 */
	R getGreatestLessThanOrEqual(int key);
}
