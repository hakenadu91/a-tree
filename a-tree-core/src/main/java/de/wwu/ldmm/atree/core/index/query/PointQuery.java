package de.wwu.ldmm.atree.core.index.query;

import de.wwu.ldmm.atree.core.index.HasKey;

public class PointQuery implements HasKey {

	private final int key;

	public PointQuery(final int key) {
		this.key = key;
	}

	@Override
	public int getKey() {
		return key;
	}
}
