package de.wwu.ldmm.atree.core.segmentation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;

public final class ShrinkingConeSegmentationTests {

	@Test
	public void testSimple() {
		final Iterator<Point<Integer>> points = IntStream.of(1, 5, 6, 20, 21, 23, 40)//
				.mapToObj(Point::<Integer>empty)//
				.iterator();
		final ShrinkingConeSegmentation<Integer> shrinkingConeSegmentation = new ShrinkingConeSegmentation<>(1);

		final List<Segment<Integer>> segments = shrinkingConeSegmentation.run(points);
		assertEquals(3, segments.size());

		assertEquals(3, segments.get(0).getPoints().size());
		assertEquals(1, segments.get(0).getPoints().get(0).getKey());
		assertEquals(2, segments.get(0).getPoints().get(1).getKey());
		assertEquals(3, segments.get(0).getPoints().get(2).getKey());

		assertEquals(3, segments.get(1).getPoints().size());
		assertEquals(7, segments.get(1).getPoints().get(0).getKey());
		assertEquals(8, segments.get(1).getPoints().get(1).getKey());
		assertEquals(9, segments.get(1).getPoints().get(2).getKey());

		assertEquals(1, segments.get(2).getPoints().size());
		assertEquals(20, segments.get(2).getPoints().get(0).getKey());
	}
}
