package de.wwu.ldmm.atree.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.segmentation.ShrinkingConeSegmentation;

/**
 * unit tests for our {@link ATree} implementation
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 30.04.2019
 */
public final class ATreeTests {

	@Test
	public void testBulkLoading() throws IOException {
		final BPTree<Segment<Integer>> segmentIndex = new BPTree<>(3);

		final ATree<Integer, BPTree<Segment<Integer>>> aTree = new ATree<>(//
				segmentIndex, // index for our segments
				ShrinkingConeSegmentation::new, // segmentation algorithm
				3, // error
				5 // buffer size
		);

		// generate sorted data
		final Random random = new Random();
		final List<Point<Integer>> data = IntStream.range(0, 100)//
				.map(i -> random.nextInt(1000))//
				.distinct()//
				.mapToObj(i -> new Point<>(i, 0))//
				.sorted()//
				.collect(Collectors.toList());

		// bulk loading
		aTree.load(data.iterator());

		for (final Point<Integer> point : data) {
			assertTrue(aTree.contains(point.getKey()), "the ATree doesnt know the point\n" + point.toString());
		}
	}
}
