package de.wwu.ldmm.atree.core.bptree;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import de.wwu.ldmm.atree.core.Point;

/**
 * unit tests for our {@link BPTree} implementation
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 29.04.2019
 */
public final class BPTreeTests {

	@SuppressWarnings("unchecked")
	@Test
	public void testInsertion() {
		final BPTree<Integer> bpTree = new BPTree<>(3);
		int record = 0;

		// (5)
		assertTrue(bpTree.insert(new Point<>(5, record++)));
		assertEquals(0, bpTree.get(5));
		assertTrue(bpTree.getRoot() instanceof BPLeafTreeNode);
		assertEquals(1, bpTree.getRoot().getChildren().size());
		assertEquals(5, bpTree.getRoot().getChildren().get(0).getKey());

		// (5,6)
		assertTrue(bpTree.insert(new Point<>(6, record++)));
		assertEquals(1, bpTree.get(6));
		assertTrue(bpTree.getRoot() instanceof BPLeafTreeNode);
		assertEquals(2, bpTree.getRoot().getChildren().size());
		assertEquals(5, bpTree.getRoot().getChildren().get(0).getKey());
		assertEquals(6, bpTree.getRoot().getChildren().get(1).getKey());

		// ----(5)----
		// (4)---(5,6)
		assertTrue(bpTree.insert(new Point<>(4, record++)));
		assertEquals(2, bpTree.get(4));
		assertTrue(bpTree.getRoot() instanceof BPInternalTreeNode);
		assertEquals(1, bpTree.getRoot().getChildren().size());
		assertEquals(5, bpTree.getRoot().getChildren().get(0).getKey());
		assertTrue(bpTree.getRoot().getChildren().get(0) instanceof BPInternalTreeNodePointer);

		final BPInternalTreeNodePointer<Integer> rootPointer = (BPInternalTreeNodePointer<Integer>) bpTree.getRoot()
				.getChildren().get(0);

		assertEquals(1, rootPointer.getLeft().getChildren().size());
		assertEquals(2, rootPointer.getRight().getChildren().size());
		assertEquals(4, rootPointer.getLeft().getChildren().get(0).getKey());
		assertEquals(5, rootPointer.getRight().getChildren().get(0).getKey());
		assertEquals(6, rootPointer.getRight().getChildren().get(1).getKey());

		// -----(5)-----
		// (3,4)---(5,6)
		assertTrue(bpTree.insert(new Point<Integer>(3, record++)));
		assertEquals(3, bpTree.get(3));
		assertEquals(2, rootPointer.getLeft().getChildren().size());
		assertEquals(2, rootPointer.getRight().getChildren().size());
		assertEquals(3, rootPointer.getLeft().getChildren().get(0).getKey());
		assertEquals(4, rootPointer.getLeft().getChildren().get(1).getKey());
		assertEquals(5, rootPointer.getRight().getChildren().get(0).getKey());
		assertEquals(6, rootPointer.getRight().getChildren().get(1).getKey());

		// -------(5)-----
		// (2,3,4)---(5,6)
		assertTrue(bpTree.insert(new Point<Integer>(2, record++)));
		assertEquals(4, bpTree.get(2));
		assertEquals(1, bpTree.getRoot().getChildren().size());
		assertEquals(3, rootPointer.getLeft().getChildren().size());
		assertEquals(2, rootPointer.getRight().getChildren().size());
		assertEquals(2, rootPointer.getLeft().getChildren().get(0).getKey());
		assertEquals(3, rootPointer.getLeft().getChildren().get(1).getKey());
		assertEquals(4, rootPointer.getLeft().getChildren().get(2).getKey());
		assertEquals(5, rootPointer.getRight().getChildren().get(0).getKey());
		assertEquals(6, rootPointer.getRight().getChildren().get(1).getKey());

		// --------(3,5)--------
		// (1,2)---(3,4)---(5,6)
		assertTrue(bpTree.insert(new Point<Integer>(1, record++)));
		assertEquals(5, bpTree.get(1));
		assertEquals(2, bpTree.getRoot().getChildren().size());
		assertEquals(rootPointer, bpTree.getRoot().getChildren().get(1));

		final BPInternalTreeNodePointer<Integer> leftPointer = (BPInternalTreeNodePointer<Integer>) bpTree.getRoot()
				.getChildren().get(0);

		assertEquals(2, leftPointer.getLeft().getChildren().size());
		assertEquals(1, leftPointer.getLeft().getChildren().get(0).getKey());
		assertEquals(2, leftPointer.getLeft().getChildren().get(1).getKey());

		assertEquals(leftPointer.getRight(), rootPointer.getLeft());
		assertEquals(2, leftPointer.getRight().getChildren().size());
		assertEquals(3, leftPointer.getRight().getChildren().get(0).getKey());
		assertEquals(4, leftPointer.getRight().getChildren().get(1).getKey());

		assertEquals(2, rootPointer.getRight().getChildren().size());
		assertEquals(5, rootPointer.getRight().getChildren().get(0).getKey());
		assertEquals(6, rootPointer.getRight().getChildren().get(1).getKey());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testComplexInsertion() throws IOException {
		final BPTree<Integer> bpTree = new BPTree<>(3);
		final int[] keys = { 6, 21, 246, 271, 284, 309, 343, 366, 399 /* , 418, 444, 478, 500, 532 */ };
		IntStream.of(keys)//
				.boxed()//
				.map(i -> new Point<Integer>(i, 0))//
				.forEach(bpTree::insert);
		for (final int key : keys) {
			assertTrue(bpTree.contains(key), "the b+ tree doesn't know the key " + key);
		}

		final BPInternalTreeNode<Integer> root = (BPInternalTreeNode<Integer>) bpTree.getRoot();
		assertEquals(1, root.getChildren().size());

		final BPInternalTreeNodePointer<Integer> rootPointer = root.getChildren().get(0);
		assertEquals(309, rootPointer.getKey());

		final BPInternalTreeNode<Integer> leftParent = (BPInternalTreeNode<Integer>) rootPointer.getLeft();
		assertEquals(2, leftParent.getChildren().size());
		assertEquals(21, leftParent.getChildren().get(0).getKey());
		assertEquals(271, leftParent.getChildren().get(1).getKey());

		final BPInternalTreeNodePointer<Integer> llPointer = ((BPInternalTreeNodePointer<Integer>) leftParent
				.getChildren().get(0));
		assertEquals(1, llPointer.getLeft().getChildren().size());
		assertEquals(2, llPointer.getRight().getChildren().size());
		assertEquals(6, llPointer.getLeft().getChildren().get(0).getKey());
		assertEquals(21, llPointer.getRight().getChildren().get(0).getKey());
		assertEquals(246, llPointer.getRight().getChildren().get(1).getKey());

		final BPInternalTreeNodePointer<Integer> lrPointer = ((BPInternalTreeNodePointer<Integer>) leftParent
				.getChildren().get(1));
		assertEquals(llPointer.getRight(), lrPointer.getLeft());
		assertEquals(2, lrPointer.getRight().getChildren().size());
		assertEquals(271, lrPointer.getRight().getChildren().get(0).getKey());
		assertEquals(284, lrPointer.getRight().getChildren().get(1).getKey());

		final BPInternalTreeNode<Integer> rightParent = (BPInternalTreeNode<Integer>) rootPointer.getRight();
		assertEquals(1, rightParent.getChildren().size());
		assertEquals(366, rightParent.getChildren().get(0).getKey());

		final BPInternalTreeNodePointer<Integer> rPointer = ((BPInternalTreeNodePointer<Integer>) rightParent
				.getChildren().get(0));

		assertEquals(2, rPointer.getLeft().getChildren().size());
		assertEquals(309, rPointer.getLeft().getChildren().get(0).getKey());
		assertEquals(343, rPointer.getLeft().getChildren().get(1).getKey());

		assertEquals(2, rPointer.getRight().getChildren().size());
		assertEquals(366, rPointer.getRight().getChildren().get(0).getKey());
		assertEquals(399, rPointer.getRight().getChildren().get(1).getKey());
	}
}
