package de.wwu.ldmm.atree.core.experiments;

import java.util.Iterator;
import java.util.Random;
import java.util.stream.IntStream;

import com.google.gson.GsonBuilder;

import de.wwu.ldmm.atree.core.ATree;
import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.bptree.BPLeafTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.segmentation.ShrinkingConeSegmentation;

public final class Experiments {

	@SuppressWarnings("unused")
	private class SegmentExperimentResult {
		double avgNumberOfSmallSegments;
		double avgNumberOfSegments;
		double avgSmallSegmentRatio;
		double avgSizeOfBigSegments;
		double avgATreeHeight;
		double avgBPTreeHeight;
	}

	public static void main(String[] args) {
		final int fanout = 3;
		final SegmentExperimentResult result = new Experiments().segmentExperiment(50000000, 10, fanout, 1000);
		System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(result));
	}

	public SegmentExperimentResult segmentExperiment(final int elementCount, final int datasetCount, final int fanout,
			final int error) {
		final Random random = new Random();

		int numberOfSmallSegmentsSum = 0;
		int numberOfSegmentsSum = 0;
		double localAvgSizeOfBigSegmentsSum = 0;
		int aTreeHeightSum = 0;
		int bpTreeHeightSum = 0;
		double smallSegmentRatioSum = 0D;

		for (int datasetIndex = 0; datasetIndex < datasetCount; datasetIndex++) {
			final BPTree<Integer> bpTree = new BPTree<>(fanout);

			final BPTree<Segment<Integer>> segmentIndex = new BPTree<>(3);
			final ATree<Integer, BPTree<Segment<Integer>>> aTree = new ATree<>(//
					segmentIndex, ShrinkingConeSegmentation::new, error, 0);

			final Iterator<Integer> keyIterator = IntStream.range(0, elementCount)//
					.map(i -> random.nextInt())//
					.sorted()//
					.iterator();

			final Iterator<Point<Integer>> iterator = new Iterator<Point<Integer>>() {

				@Override
				public boolean hasNext() {
					return keyIterator.hasNext();
				}

				int index = 0;

				@Override
				public Point<Integer> next() {
					if (index++ % 1000000 == 0) {
						System.out.println(index);
					}
					final Point<Integer> point = Point.empty(keyIterator.next());
					bpTree.insert(point);
					return point;
				}
			};

			aTree.load(iterator);

			numberOfSegmentsSum += segmentIndex.size();
			aTreeHeightSum += segmentIndex.getHeight();
			bpTreeHeightSum += bpTree.getHeight();

			int numberOfSmallSegments = 0;
			int sizeOfBigSegmentsSum = 0;
			BPLeafTreeNode<Segment<Integer>> leaf = segmentIndex.getFirstLeaf();
			while (leaf != null) {
				for (final Point<Segment<Integer>> segmentPoint : leaf.getChildren()) {
					final int segmentSize = segmentPoint.getRecord().getPoints().size();
					if (segmentSize <= error) {
						numberOfSmallSegments++;
					} else {
						sizeOfBigSegmentsSum += segmentSize;
					}
				}
				leaf = leaf.getNeighbourLeaf();
			}

			numberOfSmallSegmentsSum += numberOfSmallSegments;

			smallSegmentRatioSum += (double) numberOfSmallSegments / segmentIndex.size();

			localAvgSizeOfBigSegmentsSum += (double) sizeOfBigSegmentsSum / segmentIndex.size();
		}

		final SegmentExperimentResult result = new SegmentExperimentResult();
		result.avgATreeHeight = (double) aTreeHeightSum / datasetCount;
		result.avgBPTreeHeight = (double) bpTreeHeightSum / datasetCount;
		result.avgNumberOfSegments = (double) numberOfSegmentsSum / datasetCount;
		result.avgNumberOfSmallSegments = (double) numberOfSmallSegmentsSum / datasetCount;
		result.avgSizeOfBigSegments = (double) localAvgSizeOfBigSegmentsSum / datasetCount;
		result.avgSmallSegmentRatio = smallSegmentRatioSum / datasetCount;
		return result;
	}
}
