FROM maven:3.6.1-jdk-8-slim

COPY /a-tree-core /app/a-tree-core
COPY /a-tree-rest /app/a-tree-rest

WORKDIR /app/a-tree-core
RUN mvn clean install

WORKDIR /app/a-tree-rest

EXPOSE 8081

ENTRYPOINT [ "mvn" ]

CMD [ "exec:java" ]