package de.wwu.ldmm.atree.rest.dto;

import de.wwu.ldmm.atree.core.ATree;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.analysis.ATreeAnalysis;
import de.wwu.ldmm.atree.core.bptree.BPTree;

public final class AnalysisResponse {

	private final ATree<Integer, BPTree<Segment<Integer>>> aTree;

	private final ATreeAnalysis indices;

	public AnalysisResponse(final ATree<Integer, BPTree<Segment<Integer>>> aTree) {
		super();
		this.aTree = aTree;
		indices = new ATreeAnalysis(aTree);
	}

	public ATreeAnalysis getIndices() {
		return indices;
	}

	public ATree<Integer, BPTree<Segment<Integer>>> getaTree() {
		return aTree;
	}
}
