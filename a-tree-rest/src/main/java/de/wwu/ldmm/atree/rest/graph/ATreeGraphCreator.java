package de.wwu.ldmm.atree.rest.graph;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.wwu.ldmm.atree.core.ATree;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.bptree.BPInternalTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPLeafTreeNode;
import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.bptree.BPTreeNode;
import de.wwu.ldmm.atree.core.graph.Arrows;
import de.wwu.ldmm.atree.core.graph.Edge;
import de.wwu.ldmm.atree.core.graph.Graph;
import de.wwu.ldmm.atree.core.graph.Node;
import de.wwu.ldmm.atree.core.index.HasKey;

/**
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 03.05.2019
 */
public final class ATreeGraphCreator {

	public static final Graph toGraph(final ATree<Integer, BPTree<Segment<Integer>>> aTree) {
		return toGraph(aTree.getSegmentIndex());
	}

	public static final Graph toGraph(final BPTree<?> bpTree) {
		return fromBPTree(bpTree, new HashMap<>());
	}

	public static final Graph fromBPTree(final BPTree<?> bpTree, final Map<BPTreeNode<?, ?>, Integer> idContext) {
		final Graph graph = new Graph();
		fillBPTreeNodes(graph, bpTree.getRoot(), 0, idContext);
		fillBPTreeEdges(graph, bpTree.getRoot(), idContext);
		fillBPLeafEdges(graph, bpTree.getFirstLeaf(), idContext);
		return graph;
	}

	private static void fillBPLeafEdges(final Graph graph, final BPLeafTreeNode<?> leaf,
			final Map<BPTreeNode<?, ?>, Integer> idContext) {
		if (leaf == null) {
			return;
		}
		if (leaf.getNeighbourLeaf() != null) {
			graph.addEdge(new Edge(idContext.get(leaf), idContext.get(leaf.getNeighbourLeaf()), Arrows.to));
			fillBPLeafEdges(graph, leaf.getNeighbourLeaf(), idContext);
		}
	}

	private static void fillBPTreeNodes(final Graph graph, final BPTreeNode<?, ?> node, final int level,
			final Map<BPTreeNode<?, ?>, Integer> idContext) {
		final String nodeString = toString(node);
		if (!idContext.containsKey(node)) {
			idContext.put(node, idContext.size());
			graph.addNode(new Node(idContext.get(node), nodeString, level));
		}
		if (node instanceof BPInternalTreeNode) {
			((BPInternalTreeNode<?>) node).getChildren().stream().//
					flatMap(pointer -> Stream.of(pointer.getLeft(), pointer.getRight()))//
					.distinct()//
					.forEach(childNode -> fillBPTreeNodes(graph, childNode, level + 1, idContext));
		}
	}

	private static void fillBPTreeEdges(final Graph graph, final BPTreeNode<?, ?> node,
			final Map<BPTreeNode<?, ?>, Integer> idContext) {
		if (node.getParent() != null) {
			graph.addEdge(new Edge(idContext.get(node), idContext.get(node.getParent()), Arrows.from));
		}
		if (node instanceof BPInternalTreeNode) {
			((BPInternalTreeNode<?>) node).getChildren().stream().//
					flatMap(pointer -> Stream.of(pointer.getLeft(), pointer.getRight()))//
					.distinct()//
					.forEach(childNode -> fillBPTreeEdges(graph, childNode, idContext));
		}
	}

	private static String toString(final BPTreeNode<?, ?> node) {
		return node.getChildren().stream()//
				.map(HasKey.class::cast)//
				.map(HasKey::getKey)//
				.map(String::valueOf)//
				.collect(Collectors.joining(","));
	}

	private ATreeGraphCreator() {
		// ...
	}
}
