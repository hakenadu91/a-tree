package de.wwu.ldmm.atree.rest.dto;

import java.util.List;

import de.wwu.ldmm.atree.core.Point;

public final class InsertRequest {

	private String id;
	private List<Point<Integer>> data;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public List<Point<Integer>> getData() {
		return data;
	}

	public void setData(final List<Point<Integer>> data) {
		this.data = data;
	}
}
