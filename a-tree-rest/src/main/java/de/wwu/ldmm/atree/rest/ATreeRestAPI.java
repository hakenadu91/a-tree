package de.wwu.ldmm.atree.rest;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import de.wwu.ldmm.atree.core.ATree;
import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.json.ATreeGson;
import de.wwu.ldmm.atree.core.segmentation.ShrinkingConeSegmentation;
import de.wwu.ldmm.atree.rest.dto.AnalysisRequest;
import de.wwu.ldmm.atree.rest.dto.AnalysisResponse;
import de.wwu.ldmm.atree.rest.graph.ATreeGraphCreator;

/**
 * library independent rest api
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 02.05.2019
 */
public interface ATreeRestAPI {

	void createATree(String uuid, ATree<Integer, BPTree<Segment<Integer>>> aTree);

	ATree<Integer, BPTree<Segment<Integer>>> getATree(String uuid);

	Set<String> getATreeIDs();

	default String getATreeGraph(final String uuid) {
		return ATreeGson.INSTANCE.toJson(ATreeGraphCreator.toGraph(getATree(uuid)));
	}

	default String getSegment(final String uuid, final int startKey) {
		return Optional.ofNullable(getATree(uuid))//
				.map(ATree::getSegmentIndex)//
				.map(segmentIndex -> segmentIndex.get(startKey))//
				.map(ATreeGson.INSTANCE::toJson)//
				.orElseThrow(
						() -> new IllegalArgumentException("Fehlerhafter Zugriff (" + uuid + ", " + startKey + ')'));
	}

	/**
	 * creates an {@link ATree} and returns the identifier of the {@link ATree} in
	 * memory
	 * 
	 * @param b            {@link BPTree} degree
	 * @param error        {@link ATree} error
	 * @param bufferSize   size of the segment buffer
	 * @param segmentation {@link SegmentationType}
	 * @param initialData  initial data for bulk loading
	 * @return context id
	 */
	default String createATree(final int b, final int error, final int bufferSize, final SegmentationType segmentation,
			final List<Point<Integer>> initialData) {
		final BPTree<Segment<Integer>> segmentIndex = new BPTree<Segment<Integer>>(b);
		final ATree<Integer, BPTree<Segment<Integer>>> aTree = new ATree<>(segmentIndex,
				segmentation.getSegmentationAlgorithmCreator(), error, bufferSize);

		Optional.ofNullable(initialData).map(List::iterator).ifPresent(aTree::load);

		final String uuid = UUID.randomUUID().toString();
		createATree(uuid, aTree);
		return uuid;
	}

	default String analyze(final AnalysisRequest request) {
		final BPTree<Segment<Integer>> segmentIndex = new BPTree<>(request.getFanout());

		final ATree<Integer, BPTree<Segment<Integer>>> aTree = new ATree<>(//
				segmentIndex, // index for our segments
				ShrinkingConeSegmentation::new, // segmentation algorithm
				request.getError(), // error
				0 // buffer size
		);

		aTree.load(request.getKeys().stream().map(Point::<Integer>empty).iterator());

		return ATreeGson.INSTANCE.toJson(new AnalysisResponse(aTree));
	}

	/**
	 * inserts data into an {@link ATree}
	 * 
	 * @param contextId id for {@link ATree} identification (returned by
	 *                  {@link #create(List)})
	 * @param data      data which will be inserted into the tree
	 */
	default void insertIntoATree(final String contextId, final List<Point<Integer>> data) {
		final ATree<Integer, BPTree<Segment<Integer>>> aTree = getATree(contextId);
		data.stream().forEach(aTree::insert);
	}

	default int getFromATree(final String contextId, final Integer key) {
		final ATree<Integer, BPTree<Segment<Integer>>> aTree = getATree(contextId);
		return aTree.get(key);
	}
}
