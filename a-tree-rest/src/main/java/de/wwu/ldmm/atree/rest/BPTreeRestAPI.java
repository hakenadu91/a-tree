package de.wwu.ldmm.atree.rest;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.json.ATreeGson;
import de.wwu.ldmm.atree.rest.graph.ATreeGraphCreator;

public interface BPTreeRestAPI {

	void createBPTree(String uuid, BPTree<Integer> bpTree);

	BPTree<Integer> getBPTree(String uuid);

	Set<String> getBPTreeIDs();

	default String getBPTreeGraph(final String uuid) {
		return ATreeGson.INSTANCE.toJson(ATreeGraphCreator.toGraph(getBPTree(uuid)));
	}

	/**
	 * @param b           {@link BPTree} degree
	 * @param initialData initial data for bulk loading
	 * @return context id
	 */
	default String createBPTree(final int b, final List<Point<Integer>> initialData) {
		final BPTree<Integer> bpTree = new BPTree<Integer>(b);

		if (initialData != null) {
			initialData.forEach(bpTree::insert);
		}

		final String uuid = UUID.randomUUID().toString();
		createBPTree(uuid, bpTree);
		return uuid;
	}

	default void insertIntoBPTree(final String contextId, final List<Point<Integer>> data) {
		final BPTree<Integer> bpTree = getBPTree(contextId);
		data.stream().forEach(bpTree::insert);
	}

	default int getFromBPTree(final String contextId, final Integer key) {
		final BPTree<Integer> bpTree = getBPTree(contextId);
		return bpTree.get(key);
	}
}
