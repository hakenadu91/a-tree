package de.wwu.ldmm.atree.rest;

import java.util.function.Function;

import de.wwu.ldmm.atree.core.segmentation.SegmentationAlgorithm;
import de.wwu.ldmm.atree.core.segmentation.ShrinkingConeSegmentation;

public enum SegmentationType {

	SHRINKING_CONE(ShrinkingConeSegmentation::new);

	private final Function<Integer, SegmentationAlgorithm<Integer>> segmentationAlgorithmCreator;

	SegmentationType(final Function<Integer, SegmentationAlgorithm<Integer>> segmentationAlgorithmCreator) {
		this.segmentationAlgorithmCreator = segmentationAlgorithmCreator;
	}

	public Function<Integer, SegmentationAlgorithm<Integer>> getSegmentationAlgorithmCreator() {
		return segmentationAlgorithmCreator;
	}
}
