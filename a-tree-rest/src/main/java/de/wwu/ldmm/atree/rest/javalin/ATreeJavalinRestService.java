package de.wwu.ldmm.atree.rest.javalin;

import java.util.Optional;

import de.wwu.ldmm.atree.core.json.ATreeGson;
import de.wwu.ldmm.atree.rest.InMemoryATreeRestAPI;
import de.wwu.ldmm.atree.rest.dto.AnalysisRequest;
import de.wwu.ldmm.atree.rest.dto.CreateRequest;
import de.wwu.ldmm.atree.rest.dto.InsertRequest;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import io.javalin.http.Context;

/**
 * we use {@link Javalin} to expose our api via rest
 * 
 * @author Manuel Seiche
 * @author Tayfun Honluk
 * @since 02.05.2019
 */
public final class ATreeJavalinRestService extends InMemoryATreeRestAPI {

	/**
	 * core class of the javalin framework
	 */
	private Javalin javalin;

	public static void main(final String[] args) {
		new ATreeJavalinRestService().start();
	}

	/**
	 * start the rest api on port 8080 (default) or the one passed by the ATREE_PORT
	 * environment variable
	 */
	public void start() {
		if (javalin != null) {
			throw new IllegalStateException("service already started");
		}

		final int port = Optional.ofNullable(System.getenv("ATREE_PORT"))//
				.map(Integer::parseInt)//
				.orElse(8081);

		javalin = Javalin.create(JavalinConfig::enableCorsForAllOrigins)//
				.get("/atrees", this::handleGetATrees)//
				.post("/atrees", this::handleCreateATree)//
				.get("/atrees/:id", this::handleGetATree)//
				.get("/atrees/:id/graph", this::handleGetATreeGraph)//
				.get("/atrees/:id/segments", this::handleGetSegment)//
				.post("/analysis", this::handlePostAnalysis)//
				.post("/atrees/:id", this::handleInsertIntoATree)//
				.get("/bptrees/:id/graph", this::handleGetBPTreeGraph)//
				.start(port);
	}

	private Context handleGetATrees(final Context context) {
		return context//
				.contentType("application/json")//
				.result(createATreesResponse().toString())//
				.status(200);
	}

	private Context handleCreateATree(final Context context) {
		final CreateRequest request = Optional.ofNullable(context.body())//
				.map(json -> ATreeGson.INSTANCE.fromJson(json, CreateRequest.class))//
				.orElseThrow(IllegalArgumentException::new);

		final String id = createATree(request.getB(), //
				request.getError(), //
				request.getBufferSize(), //
				request.getSegmentation(), //
				request.getInitialData());

		return context.result(id).status(201);
	}

	private Context handleGetSegment(final Context context) {
		final String id = Optional.ofNullable(context.pathParam("id")).orElseThrow(IllegalArgumentException::new);
		final Integer startKey = Optional.ofNullable(context.queryParam("startKey"))//
				.map(Integer::parseInt)//
				.orElseThrow(IllegalArgumentException::new);
		return context//
				.contentType("application/json")//
				.result(getSegment(id, startKey).toString())//
				.status(200);
	}

	private Context handleGetATree(final Context context) {
		final String id = Optional.ofNullable(context.pathParam("id")).orElseThrow(IllegalArgumentException::new);
		return context//
				.contentType("application/json")//
				.result(createATreeResponse(id).toString())//
				.status(200);
	}

	private Context handleGetATreeGraph(final Context context) {
		final String id = Optional.ofNullable(context.pathParam("id")).orElseThrow(IllegalArgumentException::new);
		return context//
				.contentType("application/json")//
				.result(getATreeGraph(id).toString())//
				.status(200);
	}

	private Context handlePostAnalysis(final Context context) {
		final AnalysisRequest request;
		try {
			request = ATreeGson.INSTANCE.fromJson(context.body(), AnalysisRequest.class);
		} catch (final Exception ioException) {
			return context.status(400).result("Konnte den Request-Body nicht parsen. Bitte pr�fen.");
		}
		return context//
				.contentType("application/json")//
				.result(analyze(request))//
				.status(200);
	}

	private Context handleInsertIntoATree(final Context context) {
		final InsertRequest request = Optional.ofNullable(context.body())//
				.map(json -> ATreeGson.INSTANCE.fromJson(json, InsertRequest.class))//
				.orElseThrow(IllegalArgumentException::new);
		insertIntoATree(request.getId(), request.getData());
		return context.status(201);
	}

	private Context handleGetBPTreeGraph(final Context context) {
		final String id = Optional.ofNullable(context.pathParam("id")).orElseThrow(IllegalArgumentException::new);
		return context//
				.contentType("application/json")//
				.result(getBPTreeGraph(id).toString())//
				.status(200);
	}
}
