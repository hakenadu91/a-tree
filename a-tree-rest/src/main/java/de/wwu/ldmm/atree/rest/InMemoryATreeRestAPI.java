package de.wwu.ldmm.atree.rest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import de.wwu.ldmm.atree.core.ATree;
import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.core.Segment;
import de.wwu.ldmm.atree.core.bptree.BPTree;
import de.wwu.ldmm.atree.core.json.ATreeGson;
import de.wwu.ldmm.atree.core.segmentation.ShrinkingConeSegmentation;

public class InMemoryATreeRestAPI implements ATreeRestAPI, BPTreeRestAPI {

	/**
	 * normally a database should be used for holding the context identified by a
	 * {@link UUID} (key). For our purposes this {@link List} is more than enough.
	 */
	private final Map<String, ATree<Integer, BPTree<Segment<Integer>>>> aTreeCache = new HashMap<>();

	private final Map<String, BPTree<Integer>> bpTreeCache = new HashMap<>();

	public InMemoryATreeRestAPI() {
		super();
		final int[] exposeKeys = { 6, 7, 8, 10, 11, 12, 13, 21, 31, 38, 39, 46, 56, 64, 73, 80, 86, 96, 104, 105, 114,
				123, 132, 141, 149, 152, 159, 168, 178, 185, 195, 197, 203, 206, 207, 212, 221, 226, 232, 234, 235, 236,
				246, 255, 260, 263, 264, 271, 275, 277, 278, 281, 284, 289, 293, 298, 304, 309, 319, 321, 331, 341, 343,
				349, 352, 361, 365, 366, 375, 377, 379, 389, 399, 406, 409, 416, 417, 418, 419, 428, 436, 443, 444, 452,
				456, 466, 471, 478, 481, 482, 486, 493, 500, 504, 512, 513, 523, 532, 540, 545 };
		initializeExampleATrees(exposeKeys);
		initializeExampleBPTrees(exposeKeys);
	}

	private void initializeExampleATrees(final int[] exposeKeys) {
		final BPTree<Segment<Integer>> segmentIndex = new BPTree<>(3);

		final ATree<Integer, BPTree<Segment<Integer>>> aTree = new ATree<>(//
				segmentIndex, // index for our segments
				ShrinkingConeSegmentation::new, // segmentation algorithm
				3, // error
				5 // buffer size
		);

		// generate sorted data
		final List<Point<Integer>> data = Arrays.stream(exposeKeys)//
				.distinct()//
				.mapToObj(i -> new Point<>(i, 0))//
				.sorted()//
				.collect(Collectors.toList());

		// bulk loading
		aTree.load(data.iterator());

		aTreeCache.put("example", aTree);
	}

	private void initializeExampleBPTrees(final int[] exposeKeys) {
		final BPTree<Integer> bpTree = new BPTree<>(3);
		Arrays.stream(exposeKeys)//
				.boxed()//
				.map(i -> new Point<Integer>(i, 0))//
				.forEach(bpTree::insert);
		bpTreeCache.put("example", bpTree);
	}

	protected final JsonArray createATreesResponse() {
		final JsonArray aTrees = new JsonArray();
		getATreeIDs().stream()//
				.map(this::createATreeResponse)//
				.forEach(aTrees::add);
		return aTrees;
	}

	protected final JsonObject createATreeResponse(final String contextId) {
		final ATree<Integer, BPTree<Segment<Integer>>> aTree = getATree(contextId);
		final JsonObject response = new JsonObject();
		response.add("id", new JsonPrimitive(contextId));
		response.add("atree", new JsonParser().parse(ATreeGson.INSTANCE.toJson(aTree)));
		return response;
	}

	@Override
	public void createATree(final String uuid, final ATree<Integer, BPTree<Segment<Integer>>> aTree) {
		aTreeCache.put(uuid, aTree);
	}

	@Override
	public ATree<Integer, BPTree<Segment<Integer>>> getATree(final String uuid) {
		return aTreeCache.get(uuid);
	}

	@Override
	public Set<String> getATreeIDs() {
		return aTreeCache.keySet();
	}

	@Override
	public void createBPTree(final String uuid, final BPTree<Integer> bpTree) {
		bpTreeCache.put(uuid, bpTree);
	}

	@Override
	public BPTree<Integer> getBPTree(final String uuid) {
		return bpTreeCache.get(uuid);
	}

	@Override
	public Set<String> getBPTreeIDs() {
		return bpTreeCache.keySet();
	}
}
