package de.wwu.ldmm.atree.rest.dto;

import java.util.List;

import de.wwu.ldmm.atree.core.Point;
import de.wwu.ldmm.atree.rest.SegmentationType;

public final class CreateRequest {

	private Integer b;
	private Integer error;
	private Integer bufferSize;
	private SegmentationType segmentation;
	private List<Point<Integer>> initialData;

	public Integer getB() {
		return b;
	}

	public void setB(final Integer b) {
		this.b = b;
	}

	public Integer getError() {
		return error;
	}

	public void setError(final Integer error) {
		this.error = error;
	}

	public Integer getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(final Integer bufferSize) {
		this.bufferSize = bufferSize;
	}

	public SegmentationType getSegmentation() {
		return segmentation;
	}

	public void setSegmentation(final SegmentationType segmentation) {
		this.segmentation = segmentation;
	}

	public List<Point<Integer>> getInitialData() {
		return initialData;
	}

	public void setInitialData(final List<Point<Integer>> initialData) {
		this.initialData = initialData;
	}
}
