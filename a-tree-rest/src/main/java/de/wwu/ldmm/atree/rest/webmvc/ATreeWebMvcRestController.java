package de.wwu.ldmm.atree.rest.webmvc;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import de.wwu.ldmm.atree.rest.InMemoryATreeRestAPI;

@RestController
public final class ATreeWebMvcRestController extends InMemoryATreeRestAPI {

	@CrossOrigin
	@GetMapping("/atrees")
	public ResponseEntity<String> handleGetATrees() {
		return new ResponseEntity<>(createATreesResponse().toString(), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/atrees/{id}")
	public ResponseEntity<String> handleGetATree(final @PathVariable String id) {
		return new ResponseEntity<>(createATreeResponse(id).toString(), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/atrees/{id}/graph")
	public ResponseEntity<String> handleGetATreeGraph(final @PathVariable String id) {
		return new ResponseEntity<>(getATreeGraph(id).toString(), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/bptrees/{id}/graph")
	public ResponseEntity<String> handleGetBPTreeGraph(final @PathVariable String id) {
		return new ResponseEntity<>(getBPTreeGraph(id).toString(), HttpStatus.OK);
	}
}
