package de.wwu.ldmm.atree.rest.dto;

import java.util.List;

public final class AnalysisRequest {

	private Integer fanout;
	private Integer error;
	private List<Integer> keys;

	public Integer getFanout() {
		return fanout;
	}

	public Integer getError() {
		return error;
	}

	public List<Integer> getKeys() {
		return keys;
	}
}
